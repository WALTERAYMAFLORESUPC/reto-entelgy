package com.walterayma.comments.application.rest;

import com.walterayma.comments.application.comments.entitty.CommentsEntity;
import com.walterayma.comments.application.domain.comments.CommentsDATA;
import com.walterayma.comments.application.service.CommentsService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class appControllerTest {

    @Autowired
    CommentsEntity commentsEntity;

    CommentsService commentsServiceMock= Mockito.mock(CommentsService.class);

    @Autowired
    appController app = new appController(commentsServiceMock);

    @BeforeEach
    void setUp() {
        System.out.println("antes del test");
        CommentsEntity comments1 = new CommentsEntity();
        comments1.setId(1);
        comments1.setName_("id labore ex et quam laborum");
        comments1.setEmail("Eliseo@gardner.biz");
        comments1.setBody("\"laudantium enim quasi est quidem magnam voluptate ipsam eostempora quo necessitatibusdolor quam autem quasireiciendis et nam sapiente accusantium\"");
        CommentsEntity comments2 = new CommentsEntity();
        comments2.setId(2);
        comments2.setName_("quo vero reiciendis velit similique earum");
        comments2.setEmail("Jayne_Kuhic@sydney.com");
        comments2.setBody("\"est natus enim nihil est dolore omnis voluptatem numquamet omnis occaecati quod ullam atvoluptatem error expedita pariaturnihil sint nostrum voluptatem reiciendis et\"");
        CommentsEntity comments3 = new CommentsEntity();
        comments3.setId(3);
        comments3.setName_("odio adipisci rerum aut animi");
        comments3.setEmail("Nikita@garfield.biz");
        comments3.setBody("\"quia molestiae reprehenderit quasi aspernaturaut expedita occaecati aliquam eveniet laudantiumomnis quibusdam delectus saepe quia accusamus maiores nam estcum et ducimus et vero voluptates excepturi deleniti ratione\"");
        CommentsEntity comments4 = new CommentsEntity();
        comments4.setId(4);
        comments4.setName_("alias odio sit");
        comments4.setEmail("Lew@alysha.tv");
        comments4.setBody("\"non et atqueoccaecati deserunt quas accusantium unde odit nobis qui voluptatemquia voluptas consequuntur itaque doloret qui rerum deleniti ut occaecati\"");

        ArrayList<CommentsEntity> obj= new ArrayList<>();
        obj.add(comments1);
        obj.add(comments2);
        obj.add(comments3);
        obj.add(comments4);
        Mockito.when(commentsServiceMock.getComments()).thenReturn(obj);
    }

    @Test
    void getComments() {
        System.out.println("durante el test");
        ResponseEntity<CommentsDATA> comments =  app.getComments();
        //Se realiza el test para validar si el valor del atributo name_ del primer registro de la entidad comments contiene el valor "id labore ex et quam laborum"
        Assertions.assertEquals("id labore ex et quam laborum",comments.getBody().getComments().get(0).getName_());
    }

    @AfterEach
    void tearDown() {
        System.out.println("despues del test");
    }


}