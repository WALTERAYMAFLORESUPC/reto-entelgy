package com.walterayma.comments.application.comments.repository;

import com.walterayma.comments.application.comments.entitty.CommentsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;

public interface CommentsRepository extends JpaRepository<CommentsEntity, Integer> {

    @Query(value = "SELECT id, name_, email, body FROM comments", nativeQuery = true)
    ArrayList<CommentsEntity> findAll();
}
