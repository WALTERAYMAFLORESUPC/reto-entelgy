package com.walterayma.comments.application.service;

import com.walterayma.comments.application.comments.entitty.CommentsEntity;

import java.util.ArrayList;

public interface CommentsInterface {
    ArrayList<CommentsEntity> getComments();
}
