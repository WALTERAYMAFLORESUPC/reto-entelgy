package com.walterayma.comments.application.domain.comments;

import com.walterayma.comments.application.comments.entitty.CommentsEntity;

import java.util.ArrayList;

public class CommentsDATA {
    ArrayList<CommentsEntity> comments;
    String estado;

    public ArrayList<CommentsEntity> getComments() {
        return comments;
    }

    public void setComments(ArrayList<CommentsEntity> comments) {
        this.comments = comments;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "CommentsDATA{" +
                "comments=" + comments +
                ", estado='" + estado + '\'' +
                '}';
    }
}
