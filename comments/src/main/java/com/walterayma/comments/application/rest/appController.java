package com.walterayma.comments.application.rest;

import com.walterayma.comments.application.comments.entitty.CommentsEntity;
import com.walterayma.comments.application.domain.comments.CommentsDATA;
import com.walterayma.comments.application.service.CommentsService;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/reto")
public class appController {

    private CommentsService commentsService;

    private static final Logger logger = LoggerFactory.getLogger(CommentsService.class);
    public appController(CommentsService commentsService) {
        this.commentsService = commentsService;
    }

    @PostMapping("/comments")
    public ResponseEntity<CommentsDATA> getComments(){
        CommentsDATA data = new CommentsDATA();
        if(commentsService.getComments().size()>0){
            logger.info("success");
            data.setEstado("success");
            data.setComments(commentsService.getComments());
            logger.info(data.toString());
        }
        else{
            data.setEstado("error");
            data.setComments(null);
        }

        return ResponseEntity.ok(data);
    }

}
