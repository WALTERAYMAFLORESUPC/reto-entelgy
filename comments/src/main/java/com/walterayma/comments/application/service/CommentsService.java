package com.walterayma.comments.application.service;

import com.walterayma.comments.application.comments.entitty.CommentsEntity;
import com.walterayma.comments.application.comments.repository.CommentsRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CommentsService implements CommentsInterface {
    CommentsRepository commentsRepository;

    public CommentsService(CommentsRepository commentsRepository){
        this.commentsRepository=commentsRepository;
    }
    public ArrayList<CommentsEntity> getComments(){

        return commentsRepository.findAll();
    }
}
