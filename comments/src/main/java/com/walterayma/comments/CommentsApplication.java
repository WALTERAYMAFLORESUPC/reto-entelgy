package com.walterayma.comments;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommentsApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(CommentsApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {

	}
}
